﻿using System;
using System.Collections.Generic;
using System.Text;
using ConnectCareApp.Models.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ConnectCareApp.DataAccess.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public DbSet<HealthClass> HealthClasses { get; set; }
        public DbSet<AuthorizCode> AuthorizationCodes { get; set; }
        public DbSet<AuthorizationRequest> AuthorizationRequests { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Benefit> Benefits { get; set; }
        public DbSet<BenefitsCategory> BenefitsCategories { get; set; }

        public DbSet<ConnectCarePaymentData> ConnectCarePaymentData { get; set; }
        public DbSet<ConnectCareSponsor> ConnectCareSponsors { get; set; }
        public DbSet<ConnectCareBeneficiary> ConnectCareBeneficiaries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<EnrolleePassport> EnrolleePassports { get; set; }
        public DbSet<EnrolleePreexistingMedicalHistory> EnrolleePreexistingMedicalHistories { get; set; }
        public DbSet<AuthorizationCodes> EnrolleeVerificationCodes { get; set; }
        public DbSet<Lga> Lgas { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<MaritalStatus> MaritalStatus { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationTable> NotificationTables { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<PlanDefaultBenefit> PlanDefaultBenefits { get; set; }
        public DbSet<PreExistingMedicalHistoryList> PreExistingMedicalHistoryLists { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<ProviderLogin> ProviderLogins { get; set; }
        public DbSet<ProviderRating> ProviderRatings { get; set; }
        public DbSet<ProviderServices> ProviderServices { get; set; }
        public DbSet<ShortCodeMsg> ShortCodeMsgs { get; set; }
        public DbSet<Sms> Sms { get; set; }
        public DbSet<SmsConfig> SmsConfigs { get; set; }
        public DbSet<SponsorShipType> SponsorShipTypes { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Support> Supports { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<Enrollee> Enrollees { get; set; }
        public DbSet<Month> Months { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<AdmissionTracker> AdmissionTrackers { get; set; }
    }
}
