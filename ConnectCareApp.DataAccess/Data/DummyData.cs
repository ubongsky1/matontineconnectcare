﻿using ConnectCareApp.Models.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using Task = System.Threading.Tasks.Task;

namespace ConnectCareApp.DataAccess.Data
{
   public class DummyData
    {
        public static async Task Initialize(ApplicationDbContext context,
                              UserManager<ApplicationUser> userManager,
                              RoleManager<ApplicationRole> roleManager)
        {
            context.Database.EnsureCreated();

            String adminId1 = "";
            String adminId2 = "";

            string role1 = "Admin";
            string desc1 = "This is the administrators role";

            string role2 = "User";
            string desc2 = "This is the users role";

            string role3 = "Sponsor";
            string desc3 = "This is the sponsors role";

            string role4 = "Beneficiary";
            string desc4 = "This is the Beneficiary role";


            string password = "Petroleum11@";

            if (await roleManager.FindByNameAsync(role1) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role1, desc1, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role2) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role2, desc2, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role3) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role3, desc3, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role4) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role4, desc4, DateTime.Now));
            }

            if (await userManager.FindByNameAsync("akinbamidelet@outlook.com") == null)
            {
                var user = new ApplicationUser
                {
                    UserName= "Akinbamidele",  
                    Email = "akinbamidelet@outlook.com",
                    FullName = "Akinbamidele Akinmejiwa",
                    PhoneNumber = "09080086100",
                    FirstName= "Akinbamidele",
                    LastName= "Akinmejiwa"

                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role1);
                }
                adminId1 = user.Id;
            }



        }
    }
}
