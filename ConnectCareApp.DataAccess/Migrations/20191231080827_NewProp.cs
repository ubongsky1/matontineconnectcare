﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConnectCareApp.DataAccess.Migrations
{
    public partial class NewProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationCode",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AuthorizationCode = table.Column<string>(nullable: true),
                    AuthorizationCode1 = table.Column<string>(nullable: true),
                    PolicyNumber = table.Column<string>(nullable: true),
                    EnrolleeName = table.Column<string>(nullable: true),
                    EnrolleeCompany = table.Column<string>(nullable: true),
                    Diagnosis = table.Column<string>(nullable: true),
                    TypeofAuthorization = table.Column<string>(nullable: true),
                    EnrolleeAge = table.Column<int>(nullable: true),
                    EnrolleeId = table.Column<int>(nullable: true),
                    Plan = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    Requestername = table.Column<string>(nullable: true),
                    Requesterphone = table.Column<string>(nullable: true),
                    Provider = table.Column<int>(nullable: true),
                    Generatedby = table.Column<int>(nullable: true),
                    Authorizedby = table.Column<int>(nullable: true),
                    AcknowledgedByAuthorizer = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true),
                    Isadmission = table.Column<bool>(nullable: true),
                    AdmissionDate = table.Column<DateTime>(nullable: true),
                    DaysApprovded = table.Column<int>(nullable: true),
                    DischargeDate = table.Column<DateTime>(nullable: true),
                    AdmissionStatus = table.Column<string>(nullable: true),
                    IsNhis = table.Column<bool>(nullable: true),
                    Isdelivery = table.Column<bool>(nullable: true),
                    Deliverysmssent = table.Column<bool>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    TreatmentAuthorized = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationCode", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationRequest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Providerid = table.Column<int>(nullable: true),
                    ProviderName = table.Column<string>(nullable: true),
                    Policynumber = table.Column<string>(nullable: true),
                    Fullname = table.Column<string>(nullable: true),
                    Company = table.Column<string>(nullable: true),
                    Diagnosis = table.Column<string>(nullable: true),
                    Reasonforcode = table.Column<string>(nullable: true),
                    Isnew = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationRequest", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Bank",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Benefit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Benefitcategory = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Benefitlimit = table.Column<string>(nullable: true),
                    CategoryName = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Benefit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BenefitsCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Servicetype = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BenefitsCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConnectCareSponsor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Fullname = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Phonenumber = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    Emailsent = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    SiteId = table.Column<int>(nullable: true),
                    SubscriptionType = table.Column<string>(nullable: true),
                    SponsorId = table.Column<string>(nullable: true),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Secondarysponsor = table.Column<string>(nullable: true),
                    Zipcode = table.Column<string>(nullable: true),
                    Addon = table.Column<bool>(nullable: false),
                    SponsorStartDate = table.Column<DateTime>(nullable: true),
                    PolicyStartDate = table.Column<DateTime>(nullable: true),
                    PolicyEndDate = table.Column<DateTime>(nullable: true),
                    PolicynotificationConfig = table.Column<int>(nullable: true),
                    Instalment = table.Column<int>(nullable: true),
                    Policynumner = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Administeredby = table.Column<int>(nullable: true),
                    AdministrationDate = table.Column<DateTime>(nullable: true),
                    EnrolleeprofileId = table.Column<int>(nullable: true),
                    SponsorStrid = table.Column<string>(nullable: true),
                    Policynumber = table.Column<string>(nullable: true),
                    PushedtoMatontine = table.Column<bool>(nullable: false),
                    PushedDate = table.Column<DateTime>(nullable: true),
                    ManageSponsorPageId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectCareSponsor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enrollee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Parentid = table.Column<int>(nullable: true),
                    Parentrelationship = table.Column<int>(nullable: true),
                    Policynumber = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    Othernames = table.Column<string>(nullable: true),
                    Dob = table.Column<DateTime>(nullable: true),
                    Age = table.Column<int>(nullable: true),
                    Maritalstatus = table.Column<int>(nullable: true),
                    Occupation = table.Column<string>(nullable: true),
                    Sex = table.Column<int>(nullable: true),
                    Residentialaddress = table.Column<string>(nullable: true),
                    Stateid = table.Column<int>(nullable: true),
                    Lgaid = table.Column<int>(nullable: true),
                    Mobilenumber = table.Column<string>(nullable: true),
                    Emailaddress = table.Column<string>(nullable: true),
                    Sponsorshiptype = table.Column<int>(nullable: true),
                    Sponsorshiptypeothername = table.Column<string>(nullable: true),
                    Preexistingmedicalhistory = table.Column<string>(nullable: true),
                    Sponsorshiptypenote = table.Column<string>(nullable: true),
                    Companyid = table.Column<int>(nullable: true),
                    Subscriptionplanid = table.Column<int>(nullable: true),
                    Hasdependents = table.Column<bool>(nullable: true),
                    Specialidcardfield1 = table.Column<string>(nullable: true),
                    Specialidcardfield2 = table.Column<string>(nullable: true),
                    Specialidcardfield3 = table.Column<string>(nullable: true),
                    Staffprofileid = table.Column<int>(nullable: true),
                    Primaryprovider = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    Hasactivesubscription = table.Column<bool>(nullable: true),
                    Isexpundged = table.Column<bool>(nullable: true),
                    ExpungeNote = table.Column<string>(nullable: true),
                    Expungedby = table.Column<int>(nullable: true),
                    Dateexpunged = table.Column<DateTime>(nullable: true),
                    Createdby = table.Column<int>(nullable: true),
                    Datereceived = table.Column<DateTime>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    EnrolleePassportId = table.Column<int>(nullable: true),
                    IdCardPrinted = table.Column<bool>(nullable: true),
                    RefPolicynumber = table.Column<string>(nullable: true),
                    HasRefPolicyNumber = table.Column<bool>(nullable: true),
                    Mobilenumber2 = table.Column<string>(nullable: true),
                    LastyearBirthdaymsgsent = table.Column<int>(nullable: true),
                    Bulkjobid = table.Column<int>(nullable: true),
                    Passphrase = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enrollee", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnrolleePassport",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Imgraw = table.Column<byte[]>(nullable: true),
                    Enrolleeid = table.Column<int>(nullable: true),
                    Enrolleepolicyno = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnrolleePassport", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnrolleePreexistingMedicalHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Pemhid = table.Column<int>(nullable: true),
                    Enrolleeid = table.Column<int>(nullable: true),
                    Answeryesno = table.Column<bool>(nullable: true),
                    Answerstring = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnrolleePreexistingMedicalHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnrolleeVerificationCode",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EnrolleeId = table.Column<int>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    VerificationCode = table.Column<string>(nullable: true),
                    EncounterDate = table.Column<DateTime>(nullable: true),
                    DateAuthenticated = table.Column<DateTime>(nullable: true),
                    DateExpired = table.Column<DateTime>(nullable: true),
                    Channel = table.Column<int>(nullable: true),
                    RequestPhoneNumber = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<int>(nullable: true),
                    VisitPurpose = table.Column<int>(nullable: true),
                    ProviderId = table.Column<int>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true),
                    AuthChannel = table.Column<int>(nullable: true),
                    Pickedup = table.Column<bool>(nullable: true),
                    PickedUpById = table.Column<int>(nullable: true),
                    AttendedTo = table.Column<bool>(nullable: true),
                    DateAttendedTo = table.Column<DateTime>(nullable: true),
                    ServiceAccessed = table.Column<string>(nullable: true),
                    AuthorizationCodeGiven = table.Column<bool>(nullable: true),
                    AuthorizationCode = table.Column<string>(nullable: true),
                    AuthorizedByUserId = table.Column<int>(nullable: true),
                    DateAuthorized = table.Column<DateTime>(nullable: true),
                    AgentNote = table.Column<string>(nullable: true),
                    PostSmssent = table.Column<bool>(nullable: true),
                    Visittype = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnrolleeVerificationCode", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HealthClass",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HealthClass", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lga",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    State = table.Column<long>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lga", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Error = table.Column<byte[]>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Detail = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MaritalStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaritalStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Months",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Months", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Message = table.Column<string>(nullable: true),
                    NotificationType = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Notificationcode = table.Column<int>(nullable: true),
                    Roles = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Plan",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true),
                    PlanPageId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlanDefaultBenefit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Planid = table.Column<int>(nullable: true),
                    BenefitId = table.Column<int>(nullable: true),
                    BenefitLimit = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanDefaultBenefit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PreExistingMedicalHistoryList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreExistingMedicalHistoryList", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Provider",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    SubCode = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Phone2 = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Assignee = table.Column<int>(nullable: true),
                    Providergpscordinate = table.Column<string>(nullable: true),
                    Providerservices = table.Column<string>(nullable: true),
                    Providerplans = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    AuthorizationStatus = table.Column<int>(nullable: true),
                    AuthorizationNote = table.Column<string>(nullable: true),
                    DisapprovalNote = table.Column<string>(nullable: true),
                    AuthorizedBy = table.Column<int>(nullable: true),
                    DisapprovedBy = table.Column<int>(nullable: true),
                    AuthorizedDate = table.Column<DateTime>(nullable: true),
                    DisapprovalDate = table.Column<DateTime>(nullable: true),
                    Parentid = table.Column<long>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    StateId = table.Column<int>(nullable: true),
                    LgaId = table.Column<int>(nullable: true),
                    ProvideraccountId = table.Column<int>(nullable: true),
                    SiteId = table.Column<int>(nullable: true),
                    ProviderListPageId = table.Column<int>(nullable: true),
                    ProviderApprovalPageId = table.Column<int>(nullable: true),
                    DeletionNote = table.Column<string>(nullable: true),
                    TariffPageId = table.Column<int>(nullable: true),
                    TariffContentPageId = table.Column<int>(nullable: true),
                    ProviderTariffs = table.Column<string>(nullable: true),
                    Area = table.Column<string>(nullable: true),
                    Provideraccount2Id = table.Column<int>(nullable: true),
                    PaymentEmail1 = table.Column<string>(nullable: true),
                    PaymentEmail2 = table.Column<string>(nullable: true),
                    ProviderloginId = table.Column<int>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    IsDelisted = table.Column<bool>(nullable: true),
                    DelistNote = table.Column<string>(nullable: true),
                    Delisteddate = table.Column<DateTime>(nullable: true),
                    DelistedBy = table.Column<int>(nullable: true),
                    DelistedProviderPageId = table.Column<int>(nullable: true),
                    CompanyConsession = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provider", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProviderLogin",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Password = table.Column<string>(nullable: true),
                    Passwordchange = table.Column<bool>(nullable: true),
                    Browserid = table.Column<string>(nullable: true),
                    LastloginId = table.Column<string>(nullable: true),
                    Lastlogin = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    LastClaimSubmited = table.Column<DateTime>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    ProviderId = table.Column<int>(nullable: true),
                    SiteId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Altemail = table.Column<string>(nullable: true),
                    Altemail2 = table.Column<string>(nullable: true),
                    Altemail3 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProviderLogin", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProviderRating",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProviderId = table.Column<int>(nullable: true),
                    Rating = table.Column<int>(nullable: true),
                    FeedBack = table.Column<string>(nullable: true),
                    Fullname = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true),
                    Enrolleeid = table.Column<int>(nullable: true),
                    Dateaccessed = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProviderRating", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShortCodeMsg",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Mobile = table.Column<string>(nullable: true),
                    Msg = table.Column<string>(nullable: true),
                    MsgTime = table.Column<DateTime>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShortCodeMsg", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Msisdn = table.Column<string>(nullable: true),
                    EnrolleeId = table.Column<int>(nullable: true),
                    FromId = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    DateDelivered = table.Column<DateTime>(nullable: true),
                    ServerResponse = table.Column<string>(nullable: true),
                    ServerCode = table.Column<string>(nullable: true),
                    ServerStatus = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<int>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SmsConfig",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BdaySmsTemplate = table.Column<string>(nullable: true),
                    PreScheduleText = table.Column<bool>(nullable: true),
                    Mode = table.Column<int>(nullable: true),
                    Active = table.Column<bool>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsConfig", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SponsorshipType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Typename = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SponsorshipType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subscription",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SubscriptionCode = table.Column<string>(nullable: true),
                    CompanyId = table.Column<int>(nullable: true),
                    Companyplans = table.Column<string>(nullable: true),
                    Startdate = table.Column<DateTime>(nullable: true),
                    Duration = table.Column<int>(nullable: true),
                    Expirationdate = table.Column<DateTime>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    Createdby = table.Column<int>(nullable: true),
                    AuthorizationStatus = table.Column<int>(nullable: true),
                    AuthorizationNote = table.Column<string>(nullable: true),
                    DisapprovalNote = table.Column<string>(nullable: true),
                    TerminationNote = table.Column<string>(nullable: true),
                    Terminatedby = table.Column<int>(nullable: true),
                    AuthorizedBy = table.Column<int>(nullable: true),
                    DisapprovedBy = table.Column<int>(nullable: true),
                    AuthorizedDate = table.Column<DateTime>(nullable: true),
                    DisapprovalDate = table.Column<DateTime>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true),
                    SubscriptionMode = table.Column<string>(nullable: true),
                    Subsidiaries = table.Column<string>(nullable: true),
                    SubsidiaryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Supports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    ReplyBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<byte[]>(nullable: true),
                    PasswordSalt = table.Column<byte[]>(nullable: true),
                    CurrentEncryption = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    LastLoginDate = table.Column<DateTime>(nullable: true),
                    LoginAttempts = table.Column<int>(nullable: true),
                    ResetPasswordGuid = table.Column<Guid>(nullable: true),
                    ResetPasswordExpiry = table.Column<DateTime>(nullable: true),
                    DisableNotifications = table.Column<bool>(nullable: true),
                    LastNotificationReadDate = table.Column<DateTime>(nullable: true),
                    Uiculture = table.Column<string>(nullable: true),
                    Mobilephone = table.Column<string>(nullable: true),
                    CugMobilephone = table.Column<string>(nullable: true),
                    Dob = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserNotification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: true),
                    Target = table.Column<int>(nullable: true),
                    Read = table.Column<bool>(nullable: true),
                    ClickAction = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    RoleId = table.Column<int>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserNotification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Zone",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Countryid = table.Column<long>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zone", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionTracker",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    authorizCodeId = table.Column<int>(nullable: true),
                    PolicyNumber = table.Column<string>(nullable: true),
                    AdmissionDate = table.Column<DateTime>(nullable: true),
                    DaysApprovded = table.Column<int>(nullable: true),
                    DischargeDate = table.Column<DateTime>(nullable: true),
                    ProviderId = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CodeGivenBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionTracker", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdmissionTracker_AuthorizationCode_authorizCodeId",
                        column: x => x.authorizCodeId,
                        principalTable: "AuthorizationCode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConnectCareBeneficiary",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Fullname = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Phonenumber = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    Relationship = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: true),
                    Sponsorid = table.Column<int>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    SiteId = table.Column<int>(nullable: true),
                    BeneficiaryCat = table.Column<string>(nullable: true),
                    Passport = table.Column<byte[]>(nullable: true),
                    BeneficiaryId = table.Column<string>(nullable: true),
                    Firstname = table.Column<string>(nullable: true),
                    Dob = table.Column<DateTime>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Lga = table.Column<string>(nullable: true),
                    GuardianPhonenumber = table.Column<string>(nullable: true),
                    GuardianEmail = table.Column<string>(nullable: true),
                    SuggestedProvider = table.Column<string>(nullable: true),
                    SuggestedPlan = table.Column<string>(nullable: true),
                    PolicyNumber = table.Column<string>(nullable: true),
                    VerificationStatus = table.Column<bool>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    Addon = table.Column<bool>(nullable: true),
                    Administeredby = table.Column<int>(nullable: true),
                    AdministrationDate = table.Column<DateTime>(nullable: true),
                    ConnectCareSponsorId = table.Column<int>(nullable: true),
                    Sponsoridstring = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectCareBeneficiary", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectCareBeneficiary_ConnectCareSponsor_ConnectCareSponsorId",
                        column: x => x.ConnectCareSponsorId,
                        principalTable: "ConnectCareSponsor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProviderServices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    OpeningDays = table.Column<string>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    ProviderId = table.Column<int>(nullable: true),
                    SiteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProviderServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProviderServices_Provider_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "Provider",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConnectCarePaymentData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Paymentid = table.Column<string>(nullable: true),
                    SponsorId = table.Column<int>(nullable: true),
                    SponsorIdstring = table.Column<string>(nullable: true),
                    BeneficiaryId = table.Column<string>(nullable: true),
                    Policyid = table.Column<string>(nullable: true),
                    Amountpaid = table.Column<decimal>(nullable: true),
                    Planpurchased = table.Column<string>(nullable: true),
                    Addon = table.Column<bool>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ConnectCareBeneficiaryId = table.Column<int>(nullable: false),
                    ConnectCareSponsorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectCarePaymentData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectCarePaymentData_ConnectCareBeneficiary_ConnectCareBeneficiaryId",
                        column: x => x.ConnectCareBeneficiaryId,
                        principalTable: "ConnectCareBeneficiary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ConnectCarePaymentData_ConnectCareSponsor_ConnectCareSponsorId",
                        column: x => x.ConnectCareSponsorId,
                        principalTable: "ConnectCareSponsor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionTracker_authorizCodeId",
                table: "AdmissionTracker",
                column: "authorizCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectCareBeneficiary_ConnectCareSponsorId",
                table: "ConnectCareBeneficiary",
                column: "ConnectCareSponsorId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectCarePaymentData_ConnectCareBeneficiaryId",
                table: "ConnectCarePaymentData",
                column: "ConnectCareBeneficiaryId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectCarePaymentData_ConnectCareSponsorId",
                table: "ConnectCarePaymentData",
                column: "ConnectCareSponsorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProviderServices_ProviderId",
                table: "ProviderServices",
                column: "ProviderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdmissionTracker");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "AuthorizationRequest");

            migrationBuilder.DropTable(
                name: "Bank");

            migrationBuilder.DropTable(
                name: "Benefit");

            migrationBuilder.DropTable(
                name: "BenefitsCategory");

            migrationBuilder.DropTable(
                name: "ConnectCarePaymentData");

            migrationBuilder.DropTable(
                name: "Country");

            migrationBuilder.DropTable(
                name: "Enrollee");

            migrationBuilder.DropTable(
                name: "EnrolleePassport");

            migrationBuilder.DropTable(
                name: "EnrolleePreexistingMedicalHistory");

            migrationBuilder.DropTable(
                name: "EnrolleeVerificationCode");

            migrationBuilder.DropTable(
                name: "HealthClass");

            migrationBuilder.DropTable(
                name: "Lga");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "MaritalStatus");

            migrationBuilder.DropTable(
                name: "Months");

            migrationBuilder.DropTable(
                name: "Notification");

            migrationBuilder.DropTable(
                name: "NotificationTable");

            migrationBuilder.DropTable(
                name: "Plan");

            migrationBuilder.DropTable(
                name: "PlanDefaultBenefit");

            migrationBuilder.DropTable(
                name: "PreExistingMedicalHistoryList");

            migrationBuilder.DropTable(
                name: "ProviderLogin");

            migrationBuilder.DropTable(
                name: "ProviderRating");

            migrationBuilder.DropTable(
                name: "ProviderServices");

            migrationBuilder.DropTable(
                name: "ShortCodeMsg");

            migrationBuilder.DropTable(
                name: "Sms");

            migrationBuilder.DropTable(
                name: "SmsConfig");

            migrationBuilder.DropTable(
                name: "SponsorshipType");

            migrationBuilder.DropTable(
                name: "State");

            migrationBuilder.DropTable(
                name: "Subscription");

            migrationBuilder.DropTable(
                name: "Supports");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "UserNotification");

            migrationBuilder.DropTable(
                name: "Zone");

            migrationBuilder.DropTable(
                name: "AuthorizationCode");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "ConnectCareBeneficiary");

            migrationBuilder.DropTable(
                name: "Provider");

            migrationBuilder.DropTable(
                name: "ConnectCareSponsor");
        }
    }
}
