﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ConnectCareApp.Models.Entity
{
    [Table("AdmissionTracker")]
    public class AdmissionTracker
    {
        public int Id { get; set; }
        public AuthorizCode authorizCode { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? AdmissionDate { get; set; }
        public int? DaysApprovded { get; set; }
        public DateTime? DischargeDate { get; set; }
        public int ProviderId { get; set; }
        public string Status { get; set; }
        public bool IsDeleted { get; set; } 
        public string CodeGivenBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
