﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace ConnectCareApp.Models.Entity
{
    public class ApplicationUser : IdentityUser
    {

        public ApplicationUser() : base() { }

        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        public string FullName
        {
            get; set;
        }

    }
}
