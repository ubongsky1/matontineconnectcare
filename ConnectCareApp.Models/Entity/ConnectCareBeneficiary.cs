﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("ConnectCareBeneficiary")]
    public partial class ConnectCareBeneficiary
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Phonenumber { get; set; }
        [Display(Name = "Image")]
        public string ImageUrl { get; set; }
        public string Relationship { get; set; }
        public int? Age { get; set; }
        public int? Sponsorid { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; } 
        [DataType(DataType.Date)]
        public DateTime? UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public int? SiteId { get; set; }
        public string BeneficiaryCat { get; set; }
        public byte[] Passport { get; set; }
        public string BeneficiaryId { get; set; }
        public string Firstname { get; set; }
        [DataType(DataType.Date)]
        public DateTime? Dob { get; set; }
        public string Lastname { get; set; }
        public string Lga { get; set; }
        public string GuardianPhonenumber { get; set; }
        public string GuardianEmail { get; set; }
        public string SuggestedProvider { get; set; }
        public string SuggestedPlan { get; set; }
        public string PolicyNumber { get; set; }
        public bool VerificationStatus { get; set; }
        public bool Active { get; set; }
        public string Status { get; set; }
        public string Category { get; set; }
        public bool? Addon { get; set; }
     
        public int? Administeredby { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AdministrationDate { get; set; }
        [Display(Name = "ConnectCareSponsor")]
        public int? ConnectCareSponsorId { get; set; }
        public string Sponsoridstring { get; set; }

        public ConnectCareSponsor ConnectCareSponsor { get; set; }
    }
}
