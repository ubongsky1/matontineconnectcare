﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ConnectCareApp.Models.Entity
{
    [Table("ConnectCarePaymentData")]
    public partial class ConnectCarePaymentData
    {
        public int Id { get; set; }
        public string Paymentid { get; set; }
        public int? SponsorId { get; set; }
        public string SponsorIdstring { get; set; }
        public string BeneficiaryId { get; set; }
        public string Policyid { get; set; }
        public decimal? Amountpaid { get; set; }
        public string Planpurchased { get; set; }
        public bool? Addon { get; set; }
        [DataType(DataType.Date)]
        public DateTime? PaymentDate { get; set; }
        public Guid Guid { get; set; }
        [DataType(DataType.Date)]
        public DateTime? CreatedOn { get; set; }
        [DataType(DataType.Date)]
        public DateTime? UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("ConnectCareBeneficiary")]
        [Display(Name = "ConnectCareBeneficiary")]
        public int ConnectCareBeneficiaryId { get; set; }
        [ForeignKey("ConnectCareSponsor")]
        [Display(Name = "ConnectCareSponsor")]
        public int ConnectCareSponsorId { get; set; }
        public ConnectCareSponsor ConnectCareSponsor { get; set; }
        public ConnectCareBeneficiary ConnectCareBeneficiary { get; set; }

    }
}
