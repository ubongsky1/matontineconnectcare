﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("ConnectCareSponsor")]
    public partial class ConnectCareSponsor
    {
        public ConnectCareSponsor()
        {
            ConnectCareBeneficiary = new HashSet<ConnectCareBeneficiary>();
        }

        public int Id { get; set; }
        public string Fullname { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Phonenumber { get; set; }
        [Display(Name = "PassPort")]
        public string ImageUrl { get; set; }
        public bool Emailsent { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; } 
        [DataType(DataType.Date)]
        public DateTime? UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public int? SiteId { get; set; }
        public string SubscriptionType { get; set; }
        public string SponsorId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Secondarysponsor { get; set; }
        public string Zipcode { get; set; }
        public bool Addon { get; set; }
        [DataType(DataType.Date)]
        public DateTime? SponsorStartDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime? PolicyStartDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime? PolicyEndDate { get; set; }
        public int? PolicynotificationConfig { get; set; }
        public int? Instalment { get; set; }
        public string Policynumner { get; set; }
        public bool Active { get; set; }
        public int? Administeredby { get; set; }
        [DataType(DataType.Date)]
        public DateTime? AdministrationDate { get; set; }
        public int? EnrolleeprofileId { get; set; }
        public string SponsorStrid { get; set; }
        public string Policynumber { get; set; }
        public bool PushedtoMatontine { get; set; }
        [DataType(DataType.Date)]
        public DateTime? PushedDate { get; set; }
        public int? ManageSponsorPageId { get; set; }
        public ICollection<ConnectCareBeneficiary> ConnectCareBeneficiary { get; set; }
    }




  
  

}
