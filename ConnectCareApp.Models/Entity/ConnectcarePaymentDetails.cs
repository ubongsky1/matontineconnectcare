﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("ConnectcarePaymentDetails")]
    public partial class ConnectcarePaymentDetails
    {
        public int Id { get; set; }
        public string Paymentid { get; set; }
        public int? SponsorId { get; set; }
        public string SponsorIdstring { get; set; }
        public string BeneficiaryId { get; set; }
        public string Policyid { get; set; }
        public decimal? Amountpaid { get; set; }
        public string Planpurchased { get; set; }
        public bool? Addon { get; set; }
        public DateTime? PaymentDate { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        
    }
}
