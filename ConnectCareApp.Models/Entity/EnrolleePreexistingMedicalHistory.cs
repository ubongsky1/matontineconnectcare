﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("EnrolleePreexistingMedicalHistory")]
    public partial class EnrolleePreexistingMedicalHistory
    {
        public int Id { get; set; }
        public int? Pemhid { get; set; }
        public int? Enrolleeid { get; set; }
        public bool? Answeryesno { get; set; }
        public string Answerstring { get; set; }
        public bool? Status { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public int? SiteId { get; set; }
    }
}
