﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectCareApp.Models.Entity
{
    [Table("HealthClass")]
    public class HealthClass
    {
        [Key]

        public int Id { get; set; }

        public string Name { get; set; }

        public bool? IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
