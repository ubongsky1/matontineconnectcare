﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("Notification")]
    public partial class Notification
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string NotificationType { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public int? UserId { get; set; }
        public int? SiteId { get; set; }
    }
}
