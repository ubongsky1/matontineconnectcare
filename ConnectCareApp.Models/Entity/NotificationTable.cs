﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("NotificationTable")]
    public partial class NotificationTable
    {
        public int Id { get; set; }
        public int? Notificationcode { get; set; }
        public string Roles { get; set; }
        public string Description { get; set; }
        public bool? Active { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public int? SiteId { get; set; }
    }
}
