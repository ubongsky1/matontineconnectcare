﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("PlanDefaultBenefit")]
    public partial class PlanDefaultBenefit
    {
        public int Id { get; set; }
        public int? Planid { get; set; }
        public int? BenefitId { get; set; }
        public string BenefitLimit { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public int? SiteId { get; set; }
    }
}
