﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("ProviderRating")]
    public partial class ProviderRating
    {
        public int Id { get; set; }
        public int? ProviderId { get; set; }
        public int? Rating { get; set; }
        public string FeedBack { get; set; }
        public string Fullname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public int? SiteId { get; set; }
        public int? Enrolleeid { get; set; }
        public DateTime? Dateaccessed { get; set; }
    }
}
