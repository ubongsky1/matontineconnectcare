﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConnectCareApp.Models.Entity
{
    [Table("Sms")]
    public partial class Sms
    {
        public int Id { get; set; }
        public string Msisdn { get; set; }
        public int? EnrolleeId { get; set; }
        public string FromId { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? DateDelivered { get; set; }
        public string ServerResponse { get; set; }
        public string ServerCode { get; set; }
        public string ServerStatus { get; set; }
        public int? CreatedBy { get; set; }
        public Guid Guid { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public int? SiteId { get; set; }
    }
}
