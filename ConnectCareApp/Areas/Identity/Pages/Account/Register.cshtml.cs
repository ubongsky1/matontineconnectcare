﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using ConnectCareApp.Models.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace ConnectCareApp.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _roleManager = roleManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Text)]
            [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }
            [Required]
            [DataType(DataType.Text)]
            [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }
       
            [Required]
            [DataType(DataType.EmailAddress)]
            [MaxLength(50)]
            public string Email { get; set; }
            [Required]
            [DataType(DataType.PhoneNumber)]
            [MaxLength(50)]
            public string PhoneNumber { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [MaxLength(50)]
            public string Password { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [MaxLength(50)]
            [Display(Name = "Confirm Password")]
            public string ConfirmPassword { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "User Name")]
            public string UserName {
                get
                {
                    return FirstName + (string.IsNullOrEmpty(LastName) ? " " : ("." + (char?)LastName[0]).ToUpper());
                }
            }

            public string FullName
            {
                get { return FirstName + " " + LastName; }
            }

            [DataType(DataType.Date)]
            public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = Input.UserName,
                    Email = Input.Email,
                    PhoneNumber = Input.PhoneNumber,
                    PasswordHash = Input.Password,
                    FirstName = Input.FirstName,
                    LastName=Input.LastName,
                    FullName=Input.FullName

                };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {

                    string role = Request.Form["rdUserRole"].ToString();

                    if (role == SD.Admin)
                    {
                        await _userManager.AddToRoleAsync(user, SD.Admin);
                    }


                    else
                    {
                        if (role == SD.Sponsor)
                        {
                            await _userManager.AddToRoleAsync(user, SD.Sponsor);

                        }
                        else
                        {
                            if (role == SD.Beneficiary)
                            {
                                await _userManager.AddToRoleAsync(user, SD.Beneficiary);
                            }
                            else
                            {
                                await _userManager.AddToRoleAsync(user, SD.User);
                                await _signInManager.SignInAsync(user, isPersistent: false);
                                 return LocalRedirect(returnUrl);
                            }
                        }
                    }
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");


                    return RedirectToAction("Index", "Admin");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
