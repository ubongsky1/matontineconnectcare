﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ConnectCareApp.DataAccess.Data;
using ConnectCareApp.Models.Entity;

namespace ConnectCareApp.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConnectCareBeneficiariesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ConnectCareBeneficiariesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ConnectCareBeneficiaries
        [HttpGet]
        public IEnumerable<ConnectCareBeneficiary> GetConnectCareBeneficiaries(string firstName)
        {
            return _context.ConnectCareBeneficiaries.Where(x=>x.Firstname==firstName);
        }

        // GET: api/ConnectCareBeneficiaries/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConnectCareBeneficiary([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var connectCareBeneficiary = await _context.ConnectCareBeneficiaries.FindAsync(id);

            if (connectCareBeneficiary == null)
            {
                return NotFound();
            }

            return Ok(connectCareBeneficiary);
        }

        // PUT: api/ConnectCareBeneficiaries/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConnectCareBeneficiary([FromRoute] int id, [FromBody] ConnectCareBeneficiary connectCareBeneficiary)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != connectCareBeneficiary.Id)
            {
                return BadRequest();
            }

            _context.Entry(connectCareBeneficiary).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConnectCareBeneficiaryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ConnectCareBeneficiaries
        [HttpPost]
        public async Task<IActionResult> PostConnectCareBeneficiary([FromBody] ConnectCareBeneficiary connectCareBeneficiary)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ConnectCareBeneficiaries.Add(connectCareBeneficiary);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConnectCareBeneficiary", new { id = connectCareBeneficiary.Id }, connectCareBeneficiary);
        }

        // DELETE: api/ConnectCareBeneficiaries/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConnectCareBeneficiary([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var connectCareBeneficiary = await _context.ConnectCareBeneficiaries.FindAsync(id);
            if (connectCareBeneficiary == null)
            {
                return NotFound();
            }

            _context.ConnectCareBeneficiaries.Remove(connectCareBeneficiary);
            await _context.SaveChangesAsync();

            return Ok(connectCareBeneficiary);
        }

        private bool ConnectCareBeneficiaryExists(int id)
        {
            return _context.ConnectCareBeneficiaries.Any(e => e.Id == id);
        }
    }
}