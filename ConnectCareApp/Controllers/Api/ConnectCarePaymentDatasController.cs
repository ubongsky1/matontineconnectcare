﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ConnectCareApp.DataAccess.Data;
using ConnectCareApp.Models.Entity;

namespace ConnectCareApp.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConnectCarePaymentDatasController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ConnectCarePaymentDatasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ConnectCarePaymentDatas
        [HttpGet]
        public IEnumerable<ConnectCarePaymentData> GetConnectCarePaymentData(int? sponsorId)
        {
            return _context.ConnectCarePaymentData.Where(x=>x.ConnectCareSponsorId==sponsorId);
        }

        // GET: api/ConnectCarePaymentDatas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConnectCarePaymentData([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var connectCarePaymentData = await _context.ConnectCarePaymentData.FindAsync(id);

            if (connectCarePaymentData == null)
            {
                return NotFound();
            }

            return Ok(connectCarePaymentData);
        }

        // PUT: api/ConnectCarePaymentDatas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConnectCarePaymentData([FromRoute] int id, [FromBody] ConnectCarePaymentData connectCarePaymentData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != connectCarePaymentData.Id)
            {
                return BadRequest();
            }

            _context.Entry(connectCarePaymentData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConnectCarePaymentDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ConnectCarePaymentDatas
        [HttpPost]
        public async Task<IActionResult> PostConnectCarePaymentData([FromBody] ConnectCarePaymentData connectCarePaymentData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ConnectCarePaymentData.Add(connectCarePaymentData);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConnectCarePaymentData", new { id = connectCarePaymentData.Id }, connectCarePaymentData);
        }

        // DELETE: api/ConnectCarePaymentDatas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConnectCarePaymentData([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var connectCarePaymentData = await _context.ConnectCarePaymentData.FindAsync(id);
            if (connectCarePaymentData == null)
            {
                return NotFound();
            }

            _context.ConnectCarePaymentData.Remove(connectCarePaymentData);
            await _context.SaveChangesAsync();

            return Ok(connectCarePaymentData);
        }

        private bool ConnectCarePaymentDataExists(int id)
        {
            return _context.ConnectCarePaymentData.Any(e => e.Id == id);
        }
    }
}