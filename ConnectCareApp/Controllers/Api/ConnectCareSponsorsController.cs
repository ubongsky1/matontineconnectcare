﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ConnectCareApp.DataAccess.Data;
using ConnectCareApp.Models.Entity;

namespace ConnectCareApp.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConnectCareSponsorsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ConnectCareSponsorsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ConnectCareSponsors
        [HttpGet]
        public IEnumerable<ConnectCareSponsor> GetConnectCareSponsors(string userName)
        {
            return _context.ConnectCareSponsors.Where(x=>x.UserName==userName);
        }

        // GET: api/ConnectCareSponsors/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConnectCareSponsor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var connectCareSponsor = await _context.ConnectCareSponsors.FindAsync(id);

            if (connectCareSponsor == null)
            {
                return NotFound();
            }

            return Ok(connectCareSponsor);
        }

        // PUT: api/ConnectCareSponsors/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConnectCareSponsor([FromRoute] int id, [FromBody] ConnectCareSponsor connectCareSponsor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != connectCareSponsor.Id)
            {
                return BadRequest();
            }

            _context.Entry(connectCareSponsor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConnectCareSponsorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ConnectCareSponsors
        [HttpPost]
        public async Task<IActionResult> PostConnectCareSponsor([FromBody] ConnectCareSponsor connectCareSponsor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ConnectCareSponsors.Add(connectCareSponsor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConnectCareSponsor", new { id = connectCareSponsor.Id }, connectCareSponsor);
        }

        // DELETE: api/ConnectCareSponsors/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConnectCareSponsor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var connectCareSponsor = await _context.ConnectCareSponsors.FindAsync(id);
            if (connectCareSponsor == null)
            {
                return NotFound();
            }

            _context.ConnectCareSponsors.Remove(connectCareSponsor);
            await _context.SaveChangesAsync();

            return Ok(connectCareSponsor);
        }

        private bool ConnectCareSponsorExists(int id)
        {
            return _context.ConnectCareSponsors.Any(e => e.Id == id);
        }
    }
}