﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ConnectCareApp.DataAccess.Data;
using ConnectCareApp.Models.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting.Internal;
using ConnectCareApp.ViewModel;
using System.IO;

namespace ConnectCareApp.Controllers
{
    public class ConnectCareBeneficiariesController : Controller
    {
        private readonly ApplicationDbContext _context;
       
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly HostingEnvironment _hostingEnvironment;
        public ConnectCareBeneficiariesController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, HostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        // GET: ConnectCareBeneficiaries
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ConnectCareBeneficiaries.Include(c => c.ConnectCareSponsor);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ConnectCareBeneficiaries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var connectCareBeneficiary = await _context.ConnectCareBeneficiaries
                .Include(c => c.ConnectCareSponsor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (connectCareBeneficiary == null)
            {
                return NotFound();
            }

            return View(connectCareBeneficiary);
        }

        // GET: ConnectCareBeneficiaries/Create
        public async Task<IActionResult> Create()
        {
            var model = new BeneficiaryCreateVm();
            var user = await _userManager.GetUserAsync(User);
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors.Where(x => x.Email == user.Email), "Id", "UserName");

            return View(model);
        }

        // POST: ConnectCareBeneficiaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BeneficiaryCreateVm model)
        {
            var user = await _userManager.GetUserAsync(User);
            if (ModelState.IsValid)
            {
                var Beneficiary = new ConnectCareBeneficiary
                {
                    Id = model.Id,
                    Firstname=model.Firstname,
                    UserName = model.UserName,
                    Gender = model.Gender,
                    State = model.State,
                    Country = model.Country,
                    City = model.City,
                    Address = model.Address,
                    Email = model.Email,
                    Phonenumber = model.Phonenumber,
                    Relationship = model.Relationship,
                    Age = model.Age,
                    Lastname = model.Lastname,
                    CreatedOn = model.CreatedOn,
                    BeneficiaryCat = model.BeneficiaryCat,
                    Dob=model.Dob,
                    Lga=model.Lga,
                    GuardianPhonenumber=model.GuardianPhonenumber,
                    GuardianEmail=model.GuardianEmail,
                    SuggestedProvider=model.SuggestedProvider,
                    SuggestedPlan=model.SuggestedPlan,
                    PolicyNumber=model.PolicyNumber,
                    VerificationStatus=model.VerificationStatus,
                    Active=model.Active,
                    Status=model.Status,
                    Category=model.Category,
                    Addon=model.Addon,
                    Administeredby=model.Administeredby,
                    AdministrationDate=model.AdministrationDate,
                    ConnectCareSponsorId=model.ConnectCareSponsorId,
                    Fullname=model.FullName
                };
                if (model.ImageUrl != null && model.ImageUrl.Length > 0)
                {
                    var uploadDir = @"images/sponsor";
                    var fileName = Path.GetFileNameWithoutExtension(model.ImageUrl.FileName);
                    var extension = Path.GetExtension(model.ImageUrl.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = DateTime.UtcNow.ToString("yymmssfff") + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await model.ImageUrl.CopyToAsync(new FileStream(path, FileMode.Create));
                    Beneficiary.ImageUrl = "/" + uploadDir + "/" + fileName;
                }
                _context.Add(Beneficiary);
                await _context.SaveChangesAsync();
                return RedirectToAction("Info", "ConnectCareSponsors");
            }
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors.Where(x => x.Email == user.Email), "Id", "UserName", model.ConnectCareSponsorId);
            return View(model);
          
        }

        // GET: ConnectCareBeneficiaries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            if (id == null)
            {
                return NotFound();
            }

            var connectCareBeneficiary = await _context.ConnectCareBeneficiaries.FindAsync(id);
            if (connectCareBeneficiary == null)
            {
                return NotFound();
            }
            var model = new BeneficiaryEditVm()
            {
                Id = connectCareBeneficiary.Id,
                Firstname = connectCareBeneficiary.Firstname,
                UserName = connectCareBeneficiary.UserName,
                Gender = connectCareBeneficiary.Gender,
                State = connectCareBeneficiary.State,
                Country = connectCareBeneficiary.Country,
                City = connectCareBeneficiary.City,
                Address = connectCareBeneficiary.Address,
                Email = connectCareBeneficiary.Email,
                Phonenumber = connectCareBeneficiary.Phonenumber,
                Relationship = connectCareBeneficiary.Relationship,
                Age = connectCareBeneficiary.Age,
                Lastname = connectCareBeneficiary.Lastname,
                CreatedOn = connectCareBeneficiary.CreatedOn,
                BeneficiaryCat = connectCareBeneficiary.BeneficiaryCat,
                Dob = connectCareBeneficiary.Dob,
                Lga = connectCareBeneficiary.Lga,
                GuardianPhonenumber = connectCareBeneficiary.GuardianPhonenumber,
                GuardianEmail = connectCareBeneficiary.GuardianEmail,
                SuggestedProvider = connectCareBeneficiary.SuggestedProvider,
                SuggestedPlan = connectCareBeneficiary.SuggestedPlan,
                PolicyNumber = connectCareBeneficiary.PolicyNumber,
                VerificationStatus = connectCareBeneficiary.VerificationStatus,
                Active = connectCareBeneficiary.Active,
                Status = connectCareBeneficiary.Status,
                Category = connectCareBeneficiary.Category,
                Administeredby = connectCareBeneficiary.Administeredby,
                AdministrationDate = connectCareBeneficiary.AdministrationDate,
                ConnectCareSponsorId = connectCareBeneficiary.ConnectCareSponsorId
               
             
            };
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors.Where(x => x.Email == user.Email), "Id", "UserName", model.ConnectCareSponsorId);
            return View(model);
        }

        // POST: ConnectCareBeneficiaries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(BeneficiaryEditVm model)
        {
            var user = await _userManager.GetUserAsync(User);

            if (ModelState.IsValid)
            {
                var connectCareBeneficiary = await _context.ConnectCareBeneficiaries.FindAsync(model.Id);
                if (connectCareBeneficiary == null)
                {
                    return NotFound();
                }


                connectCareBeneficiary.Id = model.Id;              
                connectCareBeneficiary.Firstname=model.Firstname;
                connectCareBeneficiary.UserName=model.UserName;
                connectCareBeneficiary.Gender = model.Gender;
                connectCareBeneficiary.State = model.State;
                connectCareBeneficiary.Country = model.Country;
                connectCareBeneficiary.City = model.City;
                connectCareBeneficiary.Address = model.Address;
                connectCareBeneficiary.Email = model.Email;
                connectCareBeneficiary.Phonenumber = model.Phonenumber;
                connectCareBeneficiary.Relationship = model.Relationship;
                connectCareBeneficiary.Age = model.Age;
                connectCareBeneficiary.Lastname = model.Lastname;
                connectCareBeneficiary.CreatedOn = model.CreatedOn;
                connectCareBeneficiary.BeneficiaryCat = model.BeneficiaryCat;
                connectCareBeneficiary.Dob = model.Dob;
                connectCareBeneficiary.Lga = model.Lga;
                connectCareBeneficiary.GuardianPhonenumber = model.GuardianPhonenumber;
                connectCareBeneficiary.GuardianEmail = model.GuardianEmail;
                connectCareBeneficiary.SuggestedProvider = model.SuggestedProvider;
                connectCareBeneficiary.SuggestedPlan = model.SuggestedPlan;
                connectCareBeneficiary.PolicyNumber = model.PolicyNumber;
                connectCareBeneficiary.VerificationStatus = model.VerificationStatus;
                connectCareBeneficiary.Active = model.Active;
                connectCareBeneficiary.Status = model.Status;
                connectCareBeneficiary.Category = model.Category;
                connectCareBeneficiary.Administeredby = model.Administeredby;
                connectCareBeneficiary.AdministrationDate = model.AdministrationDate;
                connectCareBeneficiary.ConnectCareSponsorId = model.ConnectCareSponsorId;
                connectCareBeneficiary.Fullname = model.FullName;
                if (model.ImageUrl != null && model.ImageUrl.Length > 0)
                {
                    var uploadDir = @"images/sponsor";
                    var fileName = Path.GetFileNameWithoutExtension(model.ImageUrl.FileName);
                    var extension = Path.GetExtension(model.ImageUrl.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = DateTime.UtcNow.ToString("yymmssfff") + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await model.ImageUrl.CopyToAsync(new FileStream(path, FileMode.Create));
                    connectCareBeneficiary.ImageUrl = "/" + uploadDir + "/" + fileName;
                }

                _context.Update(connectCareBeneficiary);
                await _context.SaveChangesAsync();
                return RedirectToAction("Info", "ConnectCareSponsors");
            }
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors.Where(x => x.Email == user.Email), "Id", "UserName", model.ConnectCareSponsorId);
            return View();
        }

        // GET: ConnectCareBeneficiaries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var connectCareBeneficiary = await _context.ConnectCareBeneficiaries
                .Include(c => c.ConnectCareSponsor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (connectCareBeneficiary == null)
            {
                return NotFound();
            }

            return View(connectCareBeneficiary);
        }

        // POST: ConnectCareBeneficiaries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var connectCareBeneficiary = await _context.ConnectCareBeneficiaries.FindAsync(id);
            _context.ConnectCareBeneficiaries.Remove(connectCareBeneficiary);
            await _context.SaveChangesAsync();
            return RedirectToAction("Info", "ConnectCareSponsors");
        }

        private bool ConnectCareBeneficiaryExists(int id)
        {
            return _context.ConnectCareBeneficiaries.Any(e => e.Id == id);
        }
    }
}
