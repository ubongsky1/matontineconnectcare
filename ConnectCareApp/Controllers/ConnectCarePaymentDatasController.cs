﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ConnectCareApp.DataAccess.Data;
using ConnectCareApp.Models.Entity;

namespace ConnectCareApp.Controllers
{
    public class ConnectCarePaymentDatasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ConnectCarePaymentDatasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ConnectCarePaymentDatas
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ConnectCarePaymentData.Include(c => c.ConnectCareBeneficiary).Include(c => c.ConnectCareSponsor);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ConnectCarePaymentDatas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var connectCarePaymentData = await _context.ConnectCarePaymentData
                .Include(c => c.ConnectCareBeneficiary)
                .Include(c => c.ConnectCareSponsor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (connectCarePaymentData == null)
            {
                return NotFound();
            }

            return View(connectCarePaymentData);
        }

        // GET: ConnectCarePaymentDatas/Create
        public IActionResult Create()
        {
            ViewData["ConnectCareBeneficiaryId"] = new SelectList(_context.ConnectCareBeneficiaries, "Id", "Id");
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors, "Id", "Id");
            return View();
        }

        // POST: ConnectCarePaymentDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Paymentid,SponsorId,SponsorIdstring,BeneficiaryId,Policyid,Amountpaid,Planpurchased,Addon,PaymentDate,Guid,CreatedOn,UpdatedOn,IsDeleted,ConnectCareBeneficiaryId,ConnectCareSponsorId")] ConnectCarePaymentData connectCarePaymentData)
        {
            if (ModelState.IsValid)
            {
                _context.Add(connectCarePaymentData);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ConnectCareBeneficiaryId"] = new SelectList(_context.ConnectCareBeneficiaries, "Id", "Id", connectCarePaymentData.ConnectCareBeneficiaryId);
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors, "Id", "Id", connectCarePaymentData.ConnectCareSponsorId);
            return View(connectCarePaymentData);
        }

        // GET: ConnectCarePaymentDatas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var connectCarePaymentData = await _context.ConnectCarePaymentData.FindAsync(id);
            if (connectCarePaymentData == null)
            {
                return NotFound();
            }
            ViewData["ConnectCareBeneficiaryId"] = new SelectList(_context.ConnectCareBeneficiaries, "Id", "Id", connectCarePaymentData.ConnectCareBeneficiaryId);
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors, "Id", "Id", connectCarePaymentData.ConnectCareSponsorId);
            return View(connectCarePaymentData);
        }

        // POST: ConnectCarePaymentDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Paymentid,SponsorId,SponsorIdstring,BeneficiaryId,Policyid,Amountpaid,Planpurchased,Addon,PaymentDate,Guid,CreatedOn,UpdatedOn,IsDeleted,ConnectCareBeneficiaryId,ConnectCareSponsorId")] ConnectCarePaymentData connectCarePaymentData)
        {
            if (id != connectCarePaymentData.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(connectCarePaymentData);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConnectCarePaymentDataExists(connectCarePaymentData.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ConnectCareBeneficiaryId"] = new SelectList(_context.ConnectCareBeneficiaries, "Id", "Id", connectCarePaymentData.ConnectCareBeneficiaryId);
            ViewData["ConnectCareSponsorId"] = new SelectList(_context.ConnectCareSponsors, "Id", "Id", connectCarePaymentData.ConnectCareSponsorId);
            return View(connectCarePaymentData);
        }

        // GET: ConnectCarePaymentDatas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var connectCarePaymentData = await _context.ConnectCarePaymentData
                .Include(c => c.ConnectCareBeneficiary)
                .Include(c => c.ConnectCareSponsor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (connectCarePaymentData == null)
            {
                return NotFound();
            }

            return View(connectCarePaymentData);
        }

        // POST: ConnectCarePaymentDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var connectCarePaymentData = await _context.ConnectCarePaymentData.FindAsync(id);
            _context.ConnectCarePaymentData.Remove(connectCarePaymentData);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ConnectCarePaymentDataExists(int id)
        {
            return _context.ConnectCarePaymentData.Any(e => e.Id == id);
        }
    }
}
