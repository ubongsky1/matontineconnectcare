﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ConnectCareApp.DataAccess.Data;
using ConnectCareApp.Models.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using ConnectCareApp.ViewModel;
using System.IO;
using Microsoft.AspNetCore.Hosting.Internal;

namespace ConnectCareApp.Controllers
{
   [Authorize]
    public class ConnectCareSponsorsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly HostingEnvironment _hostingEnvironment;

        public ConnectCareSponsorsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, HostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

      
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Sponsor = _context.ConnectCareSponsors.Count();
            ViewBag.Beneficiary = _context.ConnectCareBeneficiaries.Count();
            int sponsor = _context.ConnectCareSponsors.Where(x => x.Email.Contains(user.Email)).Count();
            if (sponsor > 0)
            {

                return View(user);
            }

            return RedirectToAction(nameof(Create));
        }

        public async Task<IActionResult> Info()
        {
            var user = await _userManager.GetUserAsync(User);
           
            var BeneficiarySponsorVm = _context.ConnectCareSponsors.Include(m => m.ConnectCareBeneficiary).Single(m => m.Email == user.Email);
           
           
            if (BeneficiarySponsorVm == null)
            {
                return Content("Nothing to display");
            }

            return View("Info", BeneficiarySponsorVm.ConnectCareBeneficiary);
        }

        public IActionResult BeneficiaryCount(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewBag.Count = _context.ConnectCareBeneficiaries.Where(m => m.ConnectCareSponsorId == id).Count();

            return View();
        }
        public IActionResult NoRecords()
        {
            return View();
        }

        
        public async Task<IActionResult> Details()
        {
            var user = await _userManager.GetUserAsync(User);
            var SponsorDetails = await _context.ConnectCareSponsors
                .FirstOrDefaultAsync(m => m.UserName == user.UserName);

            if (SponsorDetails != null)
            {
                return View(SponsorDetails);
            }
            else
            {
                return RedirectToAction("NoRecords", "ConnectCareSponsors");
            }

        }

        public IActionResult Profile()
        {

            return View();
        }


        public IActionResult Create()
        {
            var model = new SponsorCreateVm();
            return View(model);
        }


        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SponsorCreateVm model)
        {
            if (ModelState.IsValid)
            {
                var Sponsors = new ConnectCareSponsor
                {
                    Id=model.Id,
                    UserName=model.UserName,
                    Gender=model.Gender,
                    Occupation=model.Occupation,
                    Country=model.Country,
                    State=model.State,
                    Address=model.Address,
                    Email=model.Email,
                    Password=model.Password,
                    Phonenumber=model.Phonenumber,
                    SubscriptionType=model.SubscriptionType,
                    Firstname=model.Firstname,
                    Lastname=model.Lastname,
                    Secondarysponsor=model.Secondarysponsor,
                    Zipcode=model.Zipcode,
                    Fullname=model.FullName
                };
                if (model.ImageUrl != null && model.ImageUrl.Length > 0)
                {
                    var uploadDir = @"images/sponsor";
                    var fileName = Path.GetFileNameWithoutExtension(model.ImageUrl.FileName);
                    var extension = Path.GetExtension(model.ImageUrl.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = DateTime.UtcNow.ToString("yymmssfff") + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await model.ImageUrl.CopyToAsync(new FileStream(path, FileMode.Create));
                    Sponsors.ImageUrl = "/" + uploadDir + "/" + fileName;
                }
                _context.Add(Sponsors);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

       
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var connectCareSponsor = await _context.ConnectCareSponsors.FindAsync(id);
            if (connectCareSponsor == null)
            {
                return NotFound();
            }
            return View(connectCareSponsor);
        }

       
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Fullname,Gender,Occupation,Country,State,Address,Email,Phonenumber,Emailsent,Guid,CreatedOn,UpdatedOn,IsDeleted,SiteId,SubscriptionType,SponsorId,Firstname,Lastname,Secondarysponsor,Zipcode,Addon,SponsorStartDate,PolicyStartDate,PolicyEndDate,PolicynotificationConfig,Instalment,Policynumner,Active,Administeredby,AdministrationDate,EnrolleeprofileId,SponsorStrid,Policynumber,PushedtoMatontine,PushedDate,ManageSponsorPageId")] ConnectCareSponsor connectCareSponsor)
        {
            if (id != connectCareSponsor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(connectCareSponsor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConnectCareSponsorExists(connectCareSponsor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(connectCareSponsor);
        }

       
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var connectCareSponsor = await _context.ConnectCareSponsors
                .FirstOrDefaultAsync(m => m.Id == id);
            if (connectCareSponsor == null)
            {
                return NotFound();
            }

            return View(connectCareSponsor);
        }

     
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var connectCareSponsor = await _context.ConnectCareSponsors.FindAsync(id);
            _context.ConnectCareSponsors.Remove(connectCareSponsor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ConnectCareSponsorExists(int id)
        {
            return _context.ConnectCareSponsors.Any(e => e.Id == id);
        }
    }
}
