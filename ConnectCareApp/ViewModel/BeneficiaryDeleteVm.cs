﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectCareApp.ViewModel
{
    public class BeneficiaryDeleteVm
    {
        public int Id { get; set; }
        public string FullName { get; set; }

    }
}
