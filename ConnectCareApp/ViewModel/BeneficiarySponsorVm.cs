﻿using ConnectCareApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectCareApp.ViewModel
{
    public class BeneficiarySponsorVm
    {
        public ConnectCareSponsor ConnectCareSponsor { get; set; }
        public ConnectCareBeneficiary ConnectCareBeneficiary { get; set; }
    }
}
