﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectCareApp.ViewModel
{
    public class SponsorCreateVm
    {
        public int Id { get; set; }
        public string FullName
        {
            get; set;
        }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string Occupation { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Phonenumber { get; set; }
        [Display(Name = "Image")]
        public IFormFile ImageUrl { get; set; }
        public string SubscriptionType { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Secondarysponsor { get; set; }
        public string Zipcode { get; set; }
    
    }
}
