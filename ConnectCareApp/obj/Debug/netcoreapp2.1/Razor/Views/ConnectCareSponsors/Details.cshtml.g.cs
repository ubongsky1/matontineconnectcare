#pragma checksum "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "87c65c261d35412016ab15f073ce580a06c586bf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ConnectCareSponsors_Details), @"mvc.1.0.view", @"/Views/ConnectCareSponsors/Details.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/ConnectCareSponsors/Details.cshtml", typeof(AspNetCore.Views_ConnectCareSponsors_Details))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\_ViewImports.cshtml"
using ConnectCareApp;

#line default
#line hidden
#line 2 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\_ViewImports.cshtml"
using ConnectCareApp.Models;

#line default
#line hidden
#line 4 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\_ViewImports.cshtml"
using ConnectCareApp.Models.Entity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"87c65c261d35412016ab15f073ce580a06c586bf", @"/Views/ConnectCareSponsors/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b3b5c5fd91cb0e2f6f377b4e2b9b56e277cb845d", @"/Views/_ViewImports.cshtml")]
    public class Views_ConnectCareSponsors_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ConnectCareApp.Models.Entity.ConnectCareSponsor>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Info", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(56, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
  
    ViewData["Title"] = "Details";
    Layout = "~/Views/Shared/_Layout1.cshtml";

#line default
#line hidden
            BeginContext(149, 75, true);
            WriteLiteral("<!-- Card -->\r\n<!-- Card -->\r\n\r\n<div class=\"col-md-12 text-center\">\r\n\r\n    ");
            EndContext();
            BeginContext(224, 95, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "39c5c22aa5954d42acced07d19bf5b0c", async() => {
                BeginContext(294, 21, true);
                WriteLiteral("List of Beneficiaries");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 12 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                           WriteLiteral(Model.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(319, 327, true);
            WriteLiteral(@"
    <br /><br />
</div>
<div class=""col-md-12 mb-4"">


    <!--Card-->
    <div class=""card"">

        <!--Card content-->
        <div class=""card-body"">

            <table class=""table table-bordered table-striped"">

                <tbody>
                    <tr>
                        <td class=""dec""> ");
            EndContext();
            BeginContext(647, 45, false);
#line 28 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                    Write(Html.DisplayNameFor(model => model.Firstname));

#line default
#line hidden
            EndContext();
            BeginContext(692, 48, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\"> ");
            EndContext();
            BeginContext(741, 41, false);
#line 29 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                    Write(Html.DisplayFor(model => model.Firstname));

#line default
#line hidden
            EndContext();
            BeginContext(782, 100, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"dec\">");
            EndContext();
            BeginContext(883, 42, false);
#line 32 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                   Write(Html.DisplayNameFor(model => model.Gender));

#line default
#line hidden
            EndContext();
            BeginContext(925, 48, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\"> ");
            EndContext();
            BeginContext(974, 38, false);
#line 33 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                    Write(Html.DisplayFor(model => model.Gender));

#line default
#line hidden
            EndContext();
            BeginContext(1012, 101, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"dec\"> ");
            EndContext();
            BeginContext(1114, 46, false);
#line 36 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                    Write(Html.DisplayNameFor(model => model.Occupation));

#line default
#line hidden
            EndContext();
            BeginContext(1160, 48, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\"> ");
            EndContext();
            BeginContext(1209, 42, false);
#line 37 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                    Write(Html.DisplayFor(model => model.Occupation));

#line default
#line hidden
            EndContext();
            BeginContext(1251, 102, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"dec\">  ");
            EndContext();
            BeginContext(1354, 43, false);
#line 40 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                     Write(Html.DisplayNameFor(model => model.Country));

#line default
#line hidden
            EndContext();
            BeginContext(1397, 49, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\">  ");
            EndContext();
            BeginContext(1447, 39, false);
#line 41 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                     Write(Html.DisplayFor(model => model.Country));

#line default
#line hidden
            EndContext();
            BeginContext(1486, 102, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"dec\">  ");
            EndContext();
            BeginContext(1589, 41, false);
#line 44 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                     Write(Html.DisplayNameFor(model => model.State));

#line default
#line hidden
            EndContext();
            BeginContext(1630, 49, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\">  ");
            EndContext();
            BeginContext(1680, 37, false);
#line 45 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                     Write(Html.DisplayFor(model => model.State));

#line default
#line hidden
            EndContext();
            BeginContext(1717, 100, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"dec\">");
            EndContext();
            BeginContext(1818, 43, false);
#line 48 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                   Write(Html.DisplayNameFor(model => model.Address));

#line default
#line hidden
            EndContext();
            BeginContext(1861, 47, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\">");
            EndContext();
            BeginContext(1909, 39, false);
#line 49 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                   Write(Html.DisplayFor(model => model.Address));

#line default
#line hidden
            EndContext();
            BeginContext(1948, 100, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"dec\">");
            EndContext();
            BeginContext(2049, 41, false);
#line 52 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                   Write(Html.DisplayNameFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(2090, 47, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\">");
            EndContext();
            BeginContext(2138, 37, false);
#line 53 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                   Write(Html.DisplayFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(2175, 101, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td class=\"dec\"> ");
            EndContext();
            BeginContext(2277, 47, false);
#line 56 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                    Write(Html.DisplayNameFor(model => model.Phonenumber));

#line default
#line hidden
            EndContext();
            BeginContext(2324, 48, true);
            WriteLiteral("</td>\r\n                        <td class=\"dec\"> ");
            EndContext();
            BeginContext(2373, 43, false);
#line 57 "C:\Users\Ubong Umoh\Desktop\important\ConnectCareApplication\ConnectCareApp\Views\ConnectCareSponsors\Details.cshtml"
                                    Write(Html.DisplayFor(model => model.Phonenumber));

#line default
#line hidden
            EndContext();
            BeginContext(2416, 181, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n\r\n\r\n\r\n                </tbody>\r\n            </table>\r\n\r\n        </div>\r\n\r\n\r\n    </div>\r\n    <!--/.Card-->\r\n\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ConnectCareApp.Models.Entity.ConnectCareSponsor> Html { get; private set; }
    }
}
#pragma warning restore 1591
